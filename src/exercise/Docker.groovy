package exercise

class Docker implements Serializable {

    def script

    Docker(script){
        this.script = script
    }

    def commitVersionUpdate(){
        script.echo 'Committing to GitLab....'
        script.withCredentials([script.usernamePassword(credentialsId: 'gitlab-credentials', usernameVariable: 'USER', passwordVariable: 'PASS')]){
            sh 'git config --global user.name "jenkins"'
            sh 'git config --global user.email "jenkins@email.com"'
            sh "git remote set-url origin https://'${script.USERNAME}':${script.PASSWORD}@gitlab.com/08-jenkins/exercises.git"
            sh 'git add .'
            sh 'git commit -m "jenkins CI version increment"'
            sh 'git push origin HEAD:main'
        }
    }

    def buildImage(String IMAGE_NAME, String DOCKER_REPO){
        script.echo 'Building image...'
        script.sh "docker build -t '${script.DOCKER_REPO}:${script.IMAGE_NAME}' ."
        script.withCredentials([script.usernamePassword(credentialsId: 'dockerhub-credentials', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
            script.sh 'docker login -u "${script.USER}" -p ${script.PASS}'
        }
    }

    def pushImage(String IMAGE_NAME, String DOCKER_REPO){
        script.echo 'Pushing image...'
        script.sh "docker push '${script.DOCKER_REPO}:${script.IMAGE_NAME}'"
    }

    def versionUpdate(){
        script.sh 'pwd'
        script.dir('app') {
            echo 'incrementing app version...'
            script.sh 'npm version patch'
            def pack = readJSON file: 'package.json' //install Pipeline Utility Step plugin
            def version = pack.version
            script.env.IMAGE_NAME = "${version}-${script.BUILD_NUMBER}"
        }
    }
}
